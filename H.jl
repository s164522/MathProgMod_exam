#************************************************************************
# Julia/JuMP model template

#************************************************************************
# Intro definitions
using JuMP, GLPK, Cbc
#************************************************************************
if Sys.iswindows()
    include("C:/Users/Bruger/Dropbox/skole/Mathematical Modelling Programming/JuliaFiles/Exam/pref_data.jl")
elseif Sys.isapple()
    include("pref_data.jl")
end
#************************************************************************
# PARAMETERS
ContractHours = [24 32 37 37 37 32 37 32 32 32 32 37 37 37 32 24 33 37 37 28]
Demand = [6 3 1; 6 3 1; 6 3 1; 6 3 1; 6 3 1; 4 2 1; 4 2 1;
          6 3 1; 6 3 1; 6 3 1; 6 3 1; 6 3 1; 4 2 1; 4 2 1] #[d, s]
D, S = size(Demand)
I = length(ContractHours)
M = D
MaxShiftAssign = [M 4 M;
                  M M 0;
                  M M 2;
                  M M 2;
                  0 3 M;
                  0 3 M;
                  0 4 4;
                  M 3 4;
                  M 5 M;
                  M M 2;
                  0 M 3;
                  M 3 M;
                  M M 4;
                  M 3 M;
                  0 M M;
                  M M 3;
                  M M M;
                  M 4 3;
                  0 1 M;
                  M 2 3]
experienced = [0 1 0 1 0 0 0 1 0 0 0 0 0 0 0 1 0 0 0 1]
z = zeros(3) # Bookkeep objective values
#************************************************************************


#************************************************************************
# Model
m = Model(with_optimizer(Cbc.Optimizer))

@variable(m, x[1:I, 1:D, 1:S], Bin)
@variable(m, p[1:I, 1:D], Bin)
@variable(m, y[1:D, 1:S], Bin)
@variable(m, zmax >= 0, Int)
@variable(m, zmin >= 0, Int)
@variable(m, w[1:I,1:D], Bin) # Count weekends with one shift
@variable(m, b[1:I, 1:D, 1:3], Bin) # 35 hours


@objective(m, Min, -(sum(x[i,d,s]*pref_shifts[i,d,s] - x[i,d,s]*not_pref_shifts[i,d,s]  for i=1:I,d=1:D,s=1:S)))

# Objective
#@objective(m, Min,
#           -1*(5*sum(x[i,d,s]*pref_shifts[i,d,s] - x[i,d,s]*not_pref_shifts[i,d,s]  for i=1:I,d=1:D,s=1:S) -
#           30*sum((1-y[d,s]) for d=1:D,s=1:S) -
#           1*(zmax-zmin) -
#           2*sum(w[i,d] for i=1:I,d=1:D)))

# Constraints
@constraint(m, [d=1:D, s=1:S],  sum(x[i,d,s] for i=1:I)                     >= Demand[d,s])

@constraint(m, [i=1:I],         sum(x[i,d,s]*8 for d=1:D,s=1:S)             <= 2 * ContractHours[i])

@constraint(m, [i=1:I, s=1:S],  sum(x[i,d,s] for d=1:D)                     <= MaxShiftAssign[i,s])

@constraint(m, [i=1:I, d=1:D],  sum(x[i,d,s] for s=1:S)                     <= 1-p[i,d])

@constraint(m, [i=1:I],         sum(p[i,d] for d=1:D)                       == 4)

@constraint(m, [i=1:I, d=1:D],  sum(x[i,d,s] for s=1:S)                     <= 1) # max one shift per day

# Enforce the y punishment for shifts without an experinced nurse
@constraint(m, [d=1:D, s=1:S],  sum(x[i,d,s] * experienced[i] for i=1:I)    >= y[d,s])

# C) Enforce zmax to be highest the highest difference in coverage from demand
#, similar for zmin
@constraint(m, [d=1:D, s=1:S],  zmax                                        >= sum(x[i,d,s] for i=1:I) - Demand[d,s])
@constraint(m, [d=1:D, s=1:S],  zmin                                        <= sum(x[i,d,s] for i=1:I) - Demand[d,s])

# D) For each 6 day, atleast one day has to be protected
@constraint(m, [i=1:I, d=1:D-5], sum(p[i,d2] for d2=d:(d+5))                >= 1)
    # For all nurses and days, the nurse mush have a protected day on that day or one of the following 5)

# E) Restrict to zero or two weekend-shifts. Add slack variable to penalize unfulfillment.
@constraint(m, [i=1:I, d=[6,13]], w[i,d] >= sum(x[i,d,s] for s=1:S) - sum(x[i,d+1,s] for s=1:S))
@constraint(m, [i=1:I, d=[6,13]], w[i,d] >= sum(x[i,d+1,s] for s=1:S) - sum(x[i,d,s] for s=1:S))

# F) 11-hour rule: If x[i,d,s] is 1, then nurse i cannot work the night before, nor the evening before if [d,s] is a day-shift (s=1)
@constraint(m, [i=1:I, d=2:D, s=1:2],
                    x[i,d,s] + 1/2*x[i,d-1,3] + 1/2*x[i,d-1,s+1]            <= 1)
        # Note: Problem has already been restricted to max one-shift PER day.
        # Equivalently:
    #@constraint(m, [i=1:I, d=2:D, s=1:2],
    #                    x[i,d,s] + 1/2*x[i,d-1,3] + (s<2 ? x[i,d-1,2] : 0)      <= 1)














# H)

# 35 hours break if only 1 isolated protected day. Davids Ark
M2 = 2
@constraint(m, [i=1:I, d=1:D], sum(b[i,d,k] for k=1:3)                                    >= p[i,d])
@constraint(m, [i=1:I, d=1:D-1, k=1], sum(x[i,d+1,s] for s=1:2)                           <= (p[i,d] - 2*b[i,d,k]+1)*M2) ## s Not defined on right hand side
@constraint(m, [i=1:I, d=2:D-1, k=2], x[i,d-1,3]+x[i,d+1,1]                               <= (p[i,d] - 2*b[i,d,k]+1)*M2)
@constraint(m, [i=1:I, d=2:D, k=3], sum(x[i,d-1,s] for s=2:3)                             <= (p[i,d] - 2*b[i,d,k]+1)*M2)


## Consecutive Days
@constraint(m, [i=1:I, d=2:D-2], 2 * ((1 - x[i,d-1,3]) + (1 - x[i,d+2,1]))           >= p[i,d] + p[i,d+1])
@constraint(m, [i=1:I, d=2:D-3], 3 * ((1 - x[i,d-1,3]) + (1 - x[i,d+3,1]))           >= p[i,d] + p[i,d+1] + p[i,d+2])
@constraint(m, [i=1:I, d=2:D-4], 4 * ((1 - x[i,d-1,3]) + (1 - x[i,d+4,1]))           >= p[i,d] + p[i,d+1] + p[i,d+2] + p[i,d+3])

## If last day is protected, two shitfts must be off
@constraint(m, [i=1:I], (1 - x[i,13,3]) + (1 - x[i,13,2])                            >= 2 * p[i,14])
## If last two days are protected
@constraint(m, [i=1:I], 1 + (1 - x[i,12,3])                                          >= p[i,14] + p[i,13])
## If last three days are protected
@constraint(m, [i=1:I], 2 + (1 - x[i,11,3])                                          >= p[i,14] + p[i,13] + p[i,12])
## If last four days are protected
@constraint(m, [i=1:I], 3 + (1 - x[i,10,3])                                          >= p[i,14] + p[i,13] + p[i,12] + p[i,11])

# If first day is protected, first two shifts of day 2 must be off
@constraint(m, [i=1:I], x[i,2,1]+x[i,2,2]                                           <= 2*(1-p[i,1]))
## If first two days are protected
@constraint(m, [i=1:I], x[i,3,1]                                                    <= (1-p[i,1]) + (1-p[i,2]))
## If first three days are protected
@constraint(m, [i=1:I], x[i,4,1]                                                    <= (1-p[i,1]) + (1-p[i,2]) + (1-p[i,3]))
## If first four days are protected
@constraint(m, [i=1:I], x[i,5,1]                                                    <= (1-p[i,1]) + (1-p[i,2]) + (1-p[i,3]) + (1-p[i,4]))

#print(m)
#************************************************************************


#************************************************************************
# Solve
optimize!(m)
println("Termination status: $(termination_status(m))")

#************************************************************************
if termination_status(m) == MOI.OPTIMAL
    println("Optimal objective value: $(objective_value(m))")
    if "Print on" == "Print off" # Print on/off
        for i=1:I
            println("")
            println("Nurse $(i):")
            for d=1:D
                if (value.(p[i,d]) == 1)
                    println("Day: $d    is Protected")
                end

                for s=1:S
                    if (value.(x[i,d,s]) == 1)
                        println("Day: $d    Shift: $s   Preference Value: $(pref_shifts[i,d,s])    Dis-preference Value:    $(not_pref_shifts[i,d,s])")
                    end
                end
                if (sum(value.(x[i,d,s2]) for s2 = 1:3) == 0 && value.(p[i,d]) == 0)
                    println("Day: $d    is day off")
                end
            end
        end

        #println("A Table, showing for each day (row), for each shift (column), the overcoverage of the demand, i.e. how many extra nurses there are on this shift, compared to the demand.")
        #println("Day        1       2       3")
        #for i=1:I
        #    print("Nurse $i    ")
        #    for d=1:D
        #        print("$(value(c[i,d]))     ")
        #    end
        #println("")
        #end


        println("")
        println("")
        println("")

        #for i=1:I
        #    print("Nurse $i    ")
        #    for d=1:D
        #        print("$(value(u[i,d]))     ")
        #    end
        #println("")
        #end
    end
    println("Optimal objective value: $(objective_value(m))")
else
    println("No optimal solution available")
end
#************************************************************************

#************************************************************************
println("Successfull end of $(PROGRAM_FILE)")
#************************************************************************
z[1] = objective_value(m)
#************************************************************************
#************************************************************************
@objective(m, Min, sum((1-y[d,s]) for d=1:D,s=1:S))
optimize!(m)
z[2] = objective_value(m)

@objective(m, Min, (zmax-zmin))
optimize!(m)
z[3] = objective_value(m)
#************************************************************************
#************************************************************************
z # Final solution
