#************************************************************************
# Julia/JuMP model template

#************************************************************************
# Intro definitions
using JuMP, GLPK
#************************************************************************


#************************************************************************
# PARAMETERS
Alphas = ["a1", "a2", "a3"]
Betas  = [1 3 11 4;
          7 8 15 9]

I = length(Alphas)

J = size(Betas)[1]
#************************************************************************


#************************************************************************
# Model
m = Model(with_optimizer(GLPK.Optimizer))

@variable(m, x[1:I] >= 0)

# Objective
@objective(m, Max,
           sum(  for i=1:I) )

# Constraints ...
@constraint(m, [i=1:I], x[i] <= 7 )

print(m)
#************************************************************************


#************************************************************************
# Solve
optimize!(m)
println("Termination status: $(termination_status(m))")
#************************************************************************

#************************************************************************
if termination_status(m) == MOI.OPTIMAL
    println("Optimal objective value: $(objective_value(m))")
    for v = [x] # Variables
        for i = 1:I,j=1:J
            println("$(v[i,j]): $(value(v[i,j]))")
        end
    end
    println("Optimal objective value: $(objective_value(m))")
else
    println("No optimal solution available")
end
#************************************************************************

#************************************************************************
println("Successfull end of $(PROGRAM_FILE)")
#************************************************************************
