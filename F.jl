#************************************************************************
# Julia/JuMP model template

#************************************************************************
# Intro definitions
using JuMP, GLPK, Cbc
#************************************************************************
if Sys.iswindows()
    include("C:/Users/Bruger/Dropbox/skole/Mathematical Modelling Programming/JuliaFiles/Exam/pref_data.jl")
elseif Sys.isapple()
    include("pref_data.jl")
end
#************************************************************************
# PARAMETERS
ContractHours = [24 32 37 37 37 32 37 32 32 32 32 37 37 37 32 24 33 37 37 28]
Demand = [6 3 1; 6 3 1; 6 3 1; 6 3 1; 6 3 1; 4 2 1; 4 2 1;
          6 3 1; 6 3 1; 6 3 1; 6 3 1; 6 3 1; 4 2 1; 4 2 1] #[d, s]
D, S = size(Demand)
I = length(ContractHours)
M = D
MaxShiftAssign = [M 4 M;
                  M M 0;
                  M M 2;
                  M M 2;
                  0 3 M;
                  0 3 M;
                  0 4 4;
                  M 3 4;
                  M 5 M;
                  M M 2;
                  0 M 3;
                  M 3 M;
                  M M 4;
                  M 3 M;
                  0 M M;
                  M M 3;
                  M M M;
                  M 4 3;
                  0 1 M;
                  M 2 3]
experienced = [0 1 0 1 0 0 0 1 0 0 0 0 0 0 0 1 0 0 0 1]
#************************************************************************


#************************************************************************
# Model
m = Model(with_optimizer(Cbc.Optimizer))

@variable(m, x[1:I, 1:D, 1:S], Bin)
@variable(m, p[1:I, 1:D], Bin)
@variable(m, y[1:D, 1:S], Bin)
@variable(m, zmax >= 0, Int)
@variable(m, zmin >= 0, Int)
@variable(m, w[1:I,1:D], Bin) # Count weekends with one shift

# Objective
@objective(m, Min,
           -1*(5*sum(x[i,d,s]*pref_shifts[i,d,s] - x[i,d,s]*not_pref_shifts[i,d,s]  for i=1:I,d=1:D,s=1:S) -
           30*sum((1-y[d,s]) for d=1:D,s=1:S) -
           1*(zmax-zmin) -
           2*sum(w[i,d] for i=1:I,d=1:D)))

# Constraints
@constraint(m, [d=1:D, s=1:S],  sum(x[i,d,s] for i=1:I)                     >= Demand[d,s])

@constraint(m, [i=1:I],         sum(x[i,d,s]*8 for d=1:D,s=1:S)             <= 2 * ContractHours[i])

@constraint(m, [i=1:I, s=1:S],  sum(x[i,d,s] for d=1:D)                     <= MaxShiftAssign[i,s])

@constraint(m, [i=1:I, d=1:D],  sum(x[i,d,s] for s=1:S)                     <= 1-p[i,d])

@constraint(m, [i=1:I],         sum(p[i,d] for d=1:D)                       == 4)

@constraint(m, [i=1:I, d=1:D],  sum(x[i,d,s] for s=1:S)                     <= 1) # max one shift per day

# Enforce the y punishment for shifts without an experinced nurse
@constraint(m, [d=1:D, s=1:S],  sum(x[i,d,s] * experienced[i] for i=1:I)    >= y[d,s])

# C) Enforce zmax to be highest the highest difference in coverage from demand
#, similar for zmin
@constraint(m, [d=1:D, s=1:S],  zmax                                        >= sum(x[i,d,s] for i=1:I) - Demand[d,s])
@constraint(m, [d=1:D, s=1:S],  zmin                                        <= sum(x[i,d,s] for i=1:I) - Demand[d,s])

# D) For each 6 day, atleast one day has to be protected
@constraint(m, [i=1:I, d=1:D-5], sum(p[i,d2] for d2=d:(d+5))                >= 1)
    # For all nurses and days, the nurse mush have a protected day on that day or one of the following 5)

# E) Restrict to zero or two weekend-shifts. Add slack variable to penalize unfulfillment.
@constraint(m, [i=1:I, d=[6,13]], w[i,d] >= sum(x[i,d,s] for s=1:S) - sum(x[i,d+1,s] for s=1:S))
@constraint(m, [i=1:I, d=[6,13]], w[i,d] >= sum(x[i,d+1,s] for s=1:S) - sum(x[i,d,s] for s=1:S))

# F) 11-hour rule: If x[i,d,s] is 1, then nurse i cannot work the night before, nor the evening before if [d,s] is a day-shift (s=1)
@constraint(m, [i=1:I, d=2:D, s=1:2],
                    x[i,d,s] + 1/2*x[i,d-1,3] + 1/2*x[i,d-1,s+1]            <= 1)
        # Note: Problem has already been restricted to max one-shift PER day.
        # Equivalently:
    #@constraint(m, [i=1:I, d=2:D, s=1:2],
    #                    x[i,d,s] + 1/2*x[i,d-1,3] + (s<2 ? x[i,d-1,2] : 0)      <= 1)

#print(m)
#************************************************************************


#************************************************************************
# Solve
optimize!(m)
println("Termination status: $(termination_status(m))")

#************************************************************************
if termination_status(m) == MOI.OPTIMAL
    println("Optimal objective value: $(objective_value(m))")
    if "Print on" == "Print off" # Print on/off
        println("")

        println("A Table, showing for each day (row), for each shift (column), the overcoverage of the demand, i.e. how many extra nurses there are on this shift, compared to the demand.")
        println("Day        1       2       3")
        for i=1:I
            print("Nurse $i    ")
            for d=1:D
                print("$(value(w[i,d]))     ")
            end
        println("")
        end
    end
    println("Optimal objective value: $(objective_value(m))")
else
    println("No optimal solution available")
end
#************************************************************************

#************************************************************************
println("Successfull end of $(PROGRAM_FILE)")
#************************************************************************
