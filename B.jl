#************************************************************************
# Julia/JuMP model template

#************************************************************************
# Intro definitions
using JuMP, GLPK, Cbc
#************************************************************************
if Sys.iswindows()  
    include("C:/Users/Bruger/Dropbox/skole/Mathematical Modelling Programming/JuliaFiles/Exam/pref_data.jl")
elseif Sys.isapple()
    include("pref_data.jl") 
end
#************************************************************************
# PARAMETERS
ContractHours = [24 32 37 37 37 32 37 32 32 32 32 37 37 37 32 24 33 37 37 28]
Demand = [6 3 1; 6 3 1; 6 3 1; 6 3 1; 6 3 1; 4 2 1; 4 2 1;
          6 3 1; 6 3 1; 6 3 1; 6 3 1; 6 3 1; 4 2 1; 4 2 1] #[d, s]
D, S = size(Demand)
I = length(ContractHours)
M = D
MaxShiftAssign = [M 4 M;
                  M M 0;
                  M M 2;
                  M M 2;
                  0 3 M;
                  0 3 M;
                  0 4 4;
                  M 3 4;
                  M 5 M;
                  M M 2;
                  0 M 3;
                  M 3 M;
                  M M 4;
                  M 3 M;
                  0 M M;
                  M M 3;
                  M M M;
                  M 4 3;
                  0 1 M;
                  M 2 3]
experienced = [0 1 0 1 0 0 0 1 0 0 0 0 0 0 0 1 0 0 0 1]
#************************************************************************


#************************************************************************
# Model
B = Model(with_optimizer(Cbc.Optimizer))

@variable(B, x[1:I, 1:D, 1:S], Bin)
@variable(B, p[1:I, 1:D], Bin)
@variable(B, y[1:D, 1:S], Bin)

# Objective
@objective(B, Min,
           -1*(sum(x[i,d,s]*pref_shifts[i,d,s] - x[i,d,s]*not_pref_shifts[i,d,s]  for i=1:I,d=1:D,s=1:S)
           -sum(1-y[d,s] for d=1:D,s=1:S)) )

# Constraints
@constraint(B, [d=1:D, s=1:S],  sum(x[i,d,s] for i=1:I)                     >= Demand[d,s])

@constraint(B, [i=1:I],         sum(x[i,d,s]*8 for d=1:D,s=1:S)             <= 2 * ContractHours[i])

@constraint(B, [i=1:I, s=1:S],  sum(x[i,d,s] for d=1:D)                     <= MaxShiftAssign[i,s])

@constraint(B, [i=1:I, d=1:D],  sum(x[i,d,s] for s=1:S)                     <= 1-p[i,d])

@constraint(B, [i=1:I],         sum(p[i,d] for d=1:D)                       == 4)

@constraint(B, [i=1:I, d=1:D],  sum(x[i,d,s] for s=1:S)                     <= 1) # max one shift per day

# Enforce the y punishment for shifts without an experinced nurse
@constraint(B, [d=1:D, s=1:S],  sum(x[i,d,s] * experienced[i] for i=1:I)    >= y[d,s])

#print(m)
#************************************************************************


#************************************************************************
# Solve
optimize!(B)
println("Termination status: $(termination_status(B))")
#************************************************************************

#************************************************************************
if termination_status(B) == MOI.OPTIMAL
    println("Optimal objective value: $(objective_value(B))")
    #for v = [x] # Variables
    #    for i = 1:I,d=1:D,s=1:S
    #        println("$(v[i,d,s]): $(value(v[i,d,s]))")
    #    end
    #end
    #for i=1:I
    #    println("Nurse $(i):")
    #    println("Preference met: $(sum(value(x[i,d,s])*pref_shifts[i,d,s] for d=1:D,s=1:S)),    Dis-preference: $(sum(value(x[i,d,s])*not_pref_shifts[i,d,s] for d=1:D,s=1:S))")
    #    println("Schedule: $(value.(x[i,:,:]))")
    #    println(" ")
    #end
    println("A Table, showing for each day (row), for each shift (column), how many
    experienced nurses are assigned to the shift.
    ")
    
    println("Day  Shift1  Shift2  Shift3")
    for d=1:D
        print("$d        ")
        for s=1:S
            
            print("$(convert(UInt8, sum(value.(x[i,d,s])*experienced[i] for i=1:I)))      ")

        end
        println("")
    end
    println("") 
    println("Optimal objective value: $(objective_value(B))")
else
    println("No optimal solution available")
end
#************************************************************************

#************************************************************************
println("Successfull end of $(PROGRAM_FILE)")
#************************************************************************
