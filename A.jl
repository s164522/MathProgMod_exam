#************************************************************************
# Julia/JuMP model template

#************************************************************************
# Intro definitions
using JuMP, GLPK, Cbc
#************************************************************************
if Sys.iswindows()
    include("C:/Users/Bruger/Dropbox/skole/Mathematical Modelling Programming/JuliaFiles/Exam/pref_data.jl")
elseif Sys.isapple()
    include("pref_data.jl")
end
#************************************************************************
# PARAMETERS
ContractHours = [24 32 37 37 37 32 37 32 32 32 32 37 37 37 32 24 33 37 37 28]
Demand = [6 3 1; 6 3 1; 6 3 1; 6 3 1; 6 3 1; 4 2 1; 4 2 1;
          6 3 1; 6 3 1; 6 3 1; 6 3 1; 6 3 1; 4 2 1; 4 2 1] #[d, s]
D, S = size(Demand)
I = length(ContractHours)
M = D*S
MaxShiftAssign = [M 4 M;
                  M M 0;
                  M M 2;
                  M M 2;
                  0 3 M;
                  0 3 M;
                  0 4 4;
                  M 3 4;
                  M 5 M;
                  M M 2;
                  0 M 3;
                  M 3 M;
                  M M 4;
                  M 3 M;
                  0 M M;
                  M M 3;
                  M M M;
                  M 4 3;
                  0 1 M;
                  M 2 3]
#************************************************************************


#************************************************************************
# Model
m = Model(with_optimizer(Cbc.Optimizer))

@variable(m, x[1:I, 1:D, 1:S], Bin)
@variable(m, p[1:I, 1:D], Bin)

# Objective
@objective(m, Min,
           -1*sum(x[i,d,s]*pref_shifts[i,d,s] - x[i,d,s]*not_pref_shifts[i,d,s]  for i=1:I,d=1:D,s=1:S))

# Constraints
@constraint(m, [d=1:D, s=1:S],  sum(x[i,d,s] for i=1:I)                     >= Demand[d,s])

@constraint(m, [i=1:I],         sum(x[i,d,s]*8 for d=1:D,s=1:S)             <= 2 * ContractHours[i])

@constraint(m, [i=1:I, s=1:S],  sum(x[i,d,s] for d=1:D)                     <= MaxShiftAssign[i,s])

@constraint(m, [i=1:I, d=1:D],  sum(x[i,d,s] for s=1:S)                     <= 1-p[i,d])

@constraint(m, [i=1:I],         sum(p[i,d] for d=1:D)                       == 4)

@constraint(m, [i=1:I, d=1:D],  sum(x[i,d,s] for s=1:S)                     <= 1) # max one shift per day

#print(m)
#************************************************************************

#************************************************************************
# Solve
optimize!(m)
println("Termination status: $(termination_status(m))")
#************************************************************************

#************************************************************************
if termination_status(m) == MOI.OPTIMAL
    println("Optimal objective value: $(objective_value(m))")

    ### NEW ###
    println("A table showing for each nurse: Their preference and dis-preference values
    and a row shift types (Day (D), Evening (E), Night (N), Protected (P) or
    Nothing (-)) for each day of the 14 day period. Note that due to the limits
    on the work, nurses may have days where they are not working, but which
    are not protected days of. Obviously the sum of all dis-preference values
    subtracted by all the preference values for the nurses should lead to the
    overall objective value")
    for i=1:I
        println("Nurse $(i):")
        for d=1:D
            if (value.(p[i,d]) == 1)
                println("Day: $d    is Protected")
            end

            for s=1:S
                if (value.(x[i,d,s]) == 1)
                    println("Day: $d    Shift: $s   Preference Value: $(pref_shifts[i,d,s])    Dis-preference Value:    $(not_pref_shifts[i,d,s])")
                end
            end
            if (sum(value.(x[i,d,s2]) for s2 = 1:3) == 0 && value.(p[i,d]) == 0)
                println("Day: $d    is day off")
            end
        end
        println("")
        println("")

    end



    # PRINT TAB A
    println("1    2   3   4   5   6   7   8   9   10  11  12  13  14")
    for i=1:I
        print("$(i)    ")
        for d=1:D
            
            # Same as tab_a.txt
            print("& ")
            print("$(sum((value.(x[i,d,s]) * (not_pref_shifts[i,d,s]-pref_shifts[i,d,s])) for s = 1:S))")
            print("    ")
        end
        println("")
    end
    



        # PRINT TAB A 2
        println("1    2   3   4   5   6   7   8   9   10  11  12  13  14")
        for i=1:I
            print("$(i)    ")
            for d=1:D
               
                #print("& ")
                if(value.(p[i,d]) == 1)
                    print("P")
                elseif(value.(x[i,d,1]) == 1)
                    print("D")
                elseif(value.(x[i,d,2]) == 1)
                    print("E")
                elseif(value.(x[i,d,3]) == 1)
                    print("N")        
                else
                    print("-")        
                end        
                print("    ")
            end
            println("")
        end
        

    println("Optimal objective value: $(objective_value(m))")
else
    println("No optimal solution available")
end
#************************************************************************

#************************************************************************
println("Successfull end of $(PROGRAM_FILE)")
#************************************************************************
